<?php

$xml = simplexml_load_file('book.xml');

echo '<h2>Book Listing</h2>';

$list = $xml->book;
echo '<table border=2>
<tr><th>Book No</th><th>Title</th><th>Author</th><th>Price</th><th>ISBN</th><th>Category</th></tr>
';
for ($i = 0; $i < count($list); $i++) {

    echo '<tr><td align="center">' . $list[$i]->attributes()->id . '</td>';

    echo '<td align="center"> ' . $list[$i]->title . '</td>';

    echo '<td align="center"> ' . $list[$i]->author . '</td>';
    
    echo '<td align="center"> Rs. ' . $list[$i]->price . '</td>';

    echo '<td align="center">' . $list[$i]->ISBN . '</td>';

    echo '<td align="center">' . $list[$i]->category . '</td></tr>';
}

echo'</table>';
?>